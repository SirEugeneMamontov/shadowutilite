﻿/*****************************************************************************/
using ShadowUtilite.Model;
using ShadowUtilite.Utils;
/*****************************************************************************/


namespace ShadowUtilite.TestApp
{
    public class ProgramStarterTester
    {
        ProgramStarter PS { get; set; }

/*---------------------------------------------------------------------------*/

        public ProgramStarterTester ()
        {
            PS = new ProgramStarter();
        }

/*---------------------------------------------------------------------------*/

        public void runTests ()
        {
            testNotepad();
            testNotepadPP();
            testGoogleChrome();
            testEverest();
        }

/*---------------------------------------------------------------------------*/

        private void testNotepad ()
        {
            PS.PathToProgram = new NonEmptyString( @"notepad" );
            PS.startProgramInSeparateProcess();
            System.Threading.Thread.Sleep( 2000 );
            PS.closeProgram();
        }

/*---------------------------------------------------------------------------*/

        private void testNotepadPP ()
        {
            PS.PathToProgram = new NonEmptyString( @"C:\Program Files (x86)\Notepad++\notepad++.exe" );
            PS.startProgramInSeparateProcess();
            System.Threading.Thread.Sleep( 2000 );
            PS.closeProgram();
        }

/*---------------------------------------------------------------------------*/

        private void testGoogleChrome ()
        {
            PS.PathToProgram = new NonEmptyString( 
                @"C:\Users\EM\AppData\Local\Google\Chrome\Application\chrome.exe" 
            );
            PS.startProgramInSeparateProcess();
            System.Threading.Thread.Sleep( 2000 );
            PS.closeProgram();
        }

/*---------------------------------------------------------------------------*/

        private void testEverest ()
        {
            PS.PathToProgram = new NonEmptyString( @"C:\Users\EM\Desktop\EVEREST Ultimate Edition" );
            PS.startProgramInSeparateProcess();
            System.Threading.Thread.Sleep( 2000 );
            PS.closeProgram();
        }
    }
}


/*****************************************************************************/