﻿/*****************************************************************************/
using System;
/*****************************************************************************/


// NOTE: To run tests first you need to setup current TestApp project
// as a StartUp Project.

namespace ShadowUtilite.TestApp
{
    public class Runner
    {
        public static void Main ( string[] args )
        {
            runTests();
        }

/*---------------------------------------------------------------------------*/

        public static void runTests ()
        {
            ProgramStarterTester PSTester = new ProgramStarterTester();
            PSTester.runTests();

            Console.WriteLine(
                "Tests finished with exit code 0. None exception did work.\n"
            );
        }
    }
}


/*****************************************************************************/