﻿/*****************************************************************************/


namespace ShadowUtilite.Utils
{
    public abstract class Validator < T >
    {
        public T Data { get; private set; }

/*---------------------------------------------------------------------------*/

        protected Validator ( T  _value )
        {
            validateValue( _value );
            Data = _value;
        }

/*---------------------------------------------------------------------------*/

        protected virtual void validateValue ( T _value )
        {
            if ( _value == null )
                throw new System.ArgumentNullException( "No value provided!" );
        }
    }
}


/*****************************************************************************/
