﻿/*****************************************************************************/


namespace ShadowUtilite.Utils
{
    public class PositiveInt : Validator < int >
    {
        public PositiveInt ( int _value )
            :   base ( _value )
        {
        }

/*---------------------------------------------------------------------------*/

        protected override void validateValue ( int _value )
        {
            base.validateValue( _value );

            if ( _value <= 0 )
                throw new System.ArgumentException( 
                    "Zero or negative value specified!" 
                );
        }
    }
}


/*****************************************************************************/