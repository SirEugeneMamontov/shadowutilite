﻿/*****************************************************************************/
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using ShadowUtilite.Utils;
/*****************************************************************************/


namespace ShadowUtilite.Model
{
    public class Connector
    {
        public Socket SenderSocket      { get; private set; }
        public IPAddress IpAddress      { get; private set; }
        public PositiveInt Port         { get; private set; }

        public Byte[] Buffer            { get; private set; }
        public bool IsGenerateHTML      { get; private set; }

        public Thread ListeningThread   { get; private set; }

        public static readonly string DISTINGUISH_SEQUENCE = "~#%";

/*---------------------------------------------------------------------------*/

        public Connector ( NonEmptyString _ipAddress, PositiveInt _port )
        {
            SenderSocket = new Socket( 
                    AddressFamily.InterNetwork
                ,   SocketType.Stream
                ,   ProtocolType.Tcp 
            );
      
            IpAddress       = IPAddress.Parse( _ipAddress.Data );
            Port            = new PositiveInt( _port.Data );
            Buffer          = new byte[ 1024 ];
            IsGenerateHTML  = false;

            tryConnect();

            // NOTE: There we allocate a separate thread to "listen" 
            // Server for any instructs.

            ListeningThread = new Thread( receiveMessage );
        }

/*---------------------------------------------------------------------------*/

        private void tryConnect ()
        {
            try
            {
                if ( SenderSocket != null )
                    SenderSocket.Connect( IpAddress, Port.Data );

                if ( SenderSocket.Connected )
                    Console.WriteLine( "Client succesfully connected!" );
            }
            catch ( Exception _exception )
            {
                Console.WriteLine( _exception.Message );
            }
        }

/*---------------------------------------------------------------------------*/

        public void sendMessage ( string _message )
        {
            prepareBuffer();
            Buffer = Encoding.UTF8.GetBytes( _message );

            SenderSocket.Send( Buffer );
            IsGenerateHTML = false;
        }

/*---------------------------------------------------------------------------*/

        public void sendMessage ( /*HtmlWrapper _htmlFile*/ )
        {
            // TODO: Create a method for transfer HTML file via socket.
        }


/*---------------------------------------------------------------------------*/

        public void receiveMessage()
        {
            // NOTE: The purpose of this method in receiving a command from 
            // Server to generete an HTML report.

            prepareBuffer();

            try
            {
                SenderSocket.Receive( Buffer );

                string clearMessage 
                    = encodeMessage ( Encoding.UTF8.GetString( Buffer ) );
                
                if ( clearMessage == "GenerateReport" )
                    IsGenerateHTML = true;

                if ( clearMessage == "StopWorking" )
                    closeConnection();
            }
            catch ( Exception _exception )
            {
                Console.WriteLine( _exception.Message );
            }
        }

 /*---------------------------------------------------------------------------*/

        private void prepareBuffer()
        {
            // NOTE: As a default, buffer is filled by trash.

            for ( int i = 0; i < 1024; ++i )
                Buffer[ i ] = 0;
        }

/*---------------------------------------------------------------------------*/

        private string encodeMessage ( string _dirtyMessage )
        {
            // NOTE: _dirtyMessage contains usefull message with bytes of trash,
            // that wasn't used ( obviously, message length less than 1024 )
            // so to get our message, we should dispose of any trash.

            int startIndex  = _dirtyMessage.IndexOf( DISTINGUISH_SEQUENCE );
            int endIndex    = _dirtyMessage.LastIndexOf( DISTINGUISH_SEQUENCE );
            int length      = endIndex - startIndex + 1;

            return _dirtyMessage.Substring( 0, length );
        }

/*---------------------------------------------------------------------------*/

        public void closeConnection ()
        {
            if ( ListeningThread != null )
                ListeningThread.Abort();

            if ( SenderSocket != null )
            { 
                SenderSocket.Shutdown( 
                    SocketShutdown.Both /*for receive and send.*/ 
                );
                  
                SenderSocket.Close();
            }
        }
    }
}


/*****************************************************************************/