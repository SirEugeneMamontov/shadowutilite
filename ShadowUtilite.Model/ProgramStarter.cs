﻿/*****************************************************************************/
using System;
using System.Diagnostics;
using ShadowUtilite.Utils;
/*****************************************************************************/


namespace ShadowUtilite.Model
{
    public class ProgramStarter
    {
        public NonEmptyString PathToProgram { get; set; }
        public string ProgramArgs           { get; set; }

        public Process Process              { get; private set; }

/*---------------------------------------------------------------------------*/

        public ProgramStarter() {}

/*---------------------------------------------------------------------------*/

        public ProgramStarter ( string _pathToProgram, string _programArgs = "" )
        {
            PathToProgram = new NonEmptyString( _pathToProgram );

            // NOTE: In case any program needs parameters for startUp.
            // By default ProgramArgs empty.

            ProgramArgs = _programArgs;
        }

/*---------------------------------------------------------------------------*/

        public void startProgramInSeparateProcess ()
        {
            Process = new Process();
            Process.StartInfo.FileName = PathToProgram.Data;
            Process.StartInfo.Arguments = ProgramArgs;

            try
            {
                Process.Start();
            }
            catch ( Exception _exception )
            {
                Console.WriteLine( _exception.Message );
                tryStartOnceAgain();
            }
        }

/*---------------------------------------------------------------------------*/

        public void closeProgram ()
        {
            try
            {
               Process.CloseMainWindow();
                
                if ( !Process.HasExited )
                    Process.Kill();
            }
            catch ( Exception _exception )
            {
                Console.WriteLine ( _exception.Message );
            }
        }

/*---------------------------------------------------------------------------*/

        private void tryStartOnceAgain ()
        {
            Console.Write( "Try it once again: " );

            PathToProgram = new NonEmptyString ( Console.ReadLine() );
            startProgramInSeparateProcess();
        }
    }
}


/*****************************************************************************/